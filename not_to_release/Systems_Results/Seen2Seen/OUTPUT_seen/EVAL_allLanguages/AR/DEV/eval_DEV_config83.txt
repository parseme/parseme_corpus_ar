## Global evaluation
* MWE-based: P=73/97=0.7526 R=73/228=0.3202 F=0.4492
* Tok-based: P=161/209=0.7703 R=161/498=0.3233 F=0.4554

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=0/97=0%
* IAV: MWE-based: P=0/0=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=0/0=0.0000 R=0/76=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=15/228=7% pred=7/97=7%
* LVC.cause: MWE-based: P=5/7=0.7143 R=5/15=0.3333 F=0.4545
* LVC.cause: Tok-based: P=10/14=0.7143 R=10/33=0.3030 F=0.4255
* LVC.full: MWE-proportion: gold=121/228=53% pred=66/97=68%
* LVC.full: MWE-based: P=46/66=0.6970 R=46/121=0.3802 F=0.4920
* LVC.full: Tok-based: P=104/145=0.7172 R=104/266=0.3910 F=0.5061
* VID: MWE-proportion: gold=54/228=24% pred=24/97=25%
* VID: MWE-based: P=17/24=0.7083 R=17/54=0.3148 F=0.4359
* VID: Tok-based: P=36/50=0.7200 R=36/123=0.2927 F=0.4162

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=62/97=64%
* Continuous: MWE-based: P=49/62=0.7903 R=49/126=0.3889 F=0.5213
* Discontinuous: MWE-proportion: gold=102/228=45% pred=35/97=36%
* Discontinuous: MWE-based: P=24/35=0.6857 R=24/102=0.2353 F=0.3504

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=97/97=100%
* Multi-token: MWE-based: P=73/97=0.7526 R=73/227=0.3216 F=0.4506
* Single-token: MWE-proportion: gold=1/228=0% pred=0/97=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=97/97=100%
* Seen-in-train: MWE-based: P=73/97=0.7526 R=73/128=0.5703 F=0.6489
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/97=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=53/97=55%
* Variant-of-train: MWE-based: P=41/53=0.7736 R=41/74=0.5541 F=0.6457
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=44/97=45%
* Identical-to-train: MWE-based: P=32/44=0.7273 R=32/54=0.5926 F=0.6531

