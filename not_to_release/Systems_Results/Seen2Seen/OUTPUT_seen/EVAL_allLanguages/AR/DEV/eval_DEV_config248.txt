## Global evaluation
* MWE-based: P=81/118=0.6864 R=81/228=0.3553 F=0.4682
* Tok-based: P=181/257=0.7043 R=181/498=0.3635 F=0.4795

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=4/118=3%
* IAV: MWE-based: P=0/4=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=1/8=0.1250 R=1/76=0.0132 F=0.0238
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/118=7%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=13/17=0.7647 R=13/33=0.3939 F=0.5200
* LVC.full: MWE-proportion: gold=121/228=53% pred=68/118=58%
* LVC.full: MWE-based: P=46/68=0.6765 R=46/121=0.3802 F=0.4868
* LVC.full: Tok-based: P=104/151=0.6887 R=104/266=0.3910 F=0.4988
* MVC: MWE-proportion: gold=0/228=0% pred=1/118=1%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=37/118=31%
* VID: MWE-based: P=24/37=0.6486 R=24/54=0.4444 F=0.5275
* VID: Tok-based: P=52/79=0.6582 R=52/123=0.4228 F=0.5149

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=64/118=54%
* Continuous: MWE-based: P=49/64=0.7656 R=49/126=0.3889 F=0.5158
* Discontinuous: MWE-proportion: gold=102/228=45% pred=54/118=46%
* Discontinuous: MWE-based: P=32/54=0.5926 R=32/102=0.3137 F=0.4103

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=118/118=100%
* Multi-token: MWE-based: P=81/118=0.6864 R=81/227=0.3568 F=0.4696
* Single-token: MWE-proportion: gold=1/228=0% pred=0/118=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=118/118=100%
* Seen-in-train: MWE-based: P=81/118=0.6864 R=81/128=0.6328 F=0.6585
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/118=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=75/118=64%
* Variant-of-train: MWE-based: P=48/75=0.6400 R=48/74=0.6486 F=0.6443
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=43/118=36%
* Identical-to-train: MWE-based: P=33/43=0.7674 R=33/54=0.6111 F=0.6804

