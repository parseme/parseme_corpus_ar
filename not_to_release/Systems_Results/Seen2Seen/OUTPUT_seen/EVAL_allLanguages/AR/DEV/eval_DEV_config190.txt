## Global evaluation
* MWE-based: P=125/543=0.2302 R=125/228=0.5482 F=0.3243
* Tok-based: P=309/1220=0.2533 R=309/498=0.6205 F=0.3597

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=172/543=32%
* IAV: MWE-based: P=32/172=0.1860 R=32/38=0.8421 F=0.3048
* IAV: Tok-based: P=64/344=0.1860 R=64/76=0.8421 F=0.3048
* LVC.cause: MWE-proportion: gold=15/228=7% pred=15/543=3%
* LVC.cause: MWE-based: P=7/15=0.4667 R=7/15=0.4667 F=0.4667
* LVC.cause: Tok-based: P=15/33=0.4545 R=15/33=0.4545 F=0.4545
* LVC.full: MWE-proportion: gold=121/228=53% pred=258/543=48%
* LVC.full: MWE-based: P=57/258=0.2209 R=57/121=0.4711 F=0.3008
* LVC.full: Tok-based: P=155/611=0.2537 R=155/266=0.5827 F=0.3535
* MVC: MWE-proportion: gold=0/228=0% pred=1/543=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=97/543=18%
* VID: MWE-based: P=24/97=0.2474 R=24/54=0.4444 F=0.3179
* VID: Tok-based: P=57/230=0.2478 R=57/123=0.4634 F=0.3229

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=105/543=19%
* Continuous: MWE-based: P=66/105=0.6286 R=66/126=0.5238 F=0.5714
* Discontinuous: MWE-proportion: gold=102/228=45% pred=438/543=81%
* Discontinuous: MWE-based: P=59/438=0.1347 R=59/102=0.5784 F=0.2185

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=543/543=100%
* Multi-token: MWE-based: P=125/543=0.2302 R=125/227=0.5507 F=0.3247
* Single-token: MWE-proportion: gold=1/228=0% pred=0/543=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=543/543=100%
* Seen-in-train: MWE-based: P=125/543=0.2302 R=125/128=0.9766 F=0.3726
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/543=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=472/543=87%
* Variant-of-train: MWE-based: P=74/472=0.1568 R=74/74=1.0000 F=0.2711
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=71/543=13%
* Identical-to-train: MWE-based: P=51/71=0.7183 R=51/54=0.9444 F=0.8160

