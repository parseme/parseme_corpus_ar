## Global evaluation
* MWE-based: P=78/117=0.6667 R=78/228=0.3421 F=0.4522
* Tok-based: P=176/255=0.6902 R=176/498=0.3534 F=0.4675

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=3/117=3%
* IAV: MWE-based: P=0/3=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=1/6=0.1667 R=1/76=0.0132 F=0.0244
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/117=7%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=13/17=0.7647 R=13/33=0.3939 F=0.5200
* LVC.full: MWE-proportion: gold=121/228=53% pred=70/117=60%
* LVC.full: MWE-based: P=46/70=0.6571 R=46/121=0.3802 F=0.4817
* LVC.full: Tok-based: P=104/155=0.6710 R=104/266=0.3910 F=0.4941
* MVC: MWE-proportion: gold=0/228=0% pred=1/117=1%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=35/117=30%
* VID: MWE-based: P=21/35=0.6000 R=21/54=0.3889 F=0.4719
* VID: Tok-based: P=47/75=0.6267 R=47/123=0.3821 F=0.4747

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=63/117=54%
* Continuous: MWE-based: P=48/63=0.7619 R=48/126=0.3810 F=0.5079
* Discontinuous: MWE-proportion: gold=102/228=45% pred=54/117=46%
* Discontinuous: MWE-based: P=30/54=0.5556 R=30/102=0.2941 F=0.3846

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=117/117=100%
* Multi-token: MWE-based: P=78/117=0.6667 R=78/227=0.3436 F=0.4535
* Single-token: MWE-proportion: gold=1/228=0% pred=0/117=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=117/117=100%
* Seen-in-train: MWE-based: P=78/117=0.6667 R=78/128=0.6094 F=0.6367
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/117=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=73/117=62%
* Variant-of-train: MWE-based: P=46/73=0.6301 R=46/74=0.6216 F=0.6259
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=44/117=38%
* Identical-to-train: MWE-based: P=32/44=0.7273 R=32/54=0.5926 F=0.6531

