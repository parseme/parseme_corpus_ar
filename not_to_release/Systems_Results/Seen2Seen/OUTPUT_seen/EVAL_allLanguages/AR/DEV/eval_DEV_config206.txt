## Global evaluation
* MWE-based: P=106/164=0.6463 R=106/228=0.4649 F=0.5408
* Tok-based: P=236/347=0.6801 R=236/498=0.4739 F=0.5586

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=56/164=34%
* IAV: MWE-based: P=27/56=0.4821 R=27/38=0.7105 F=0.5745
* IAV: Tok-based: P=55/112=0.4911 R=55/76=0.7237 F=0.5851
* LVC.cause: MWE-proportion: gold=15/228=7% pred=7/164=4%
* LVC.cause: MWE-based: P=5/7=0.7143 R=5/15=0.3333 F=0.4545
* LVC.cause: Tok-based: P=10/14=0.7143 R=10/33=0.3030 F=0.4255
* LVC.full: MWE-proportion: gold=121/228=53% pred=74/164=45%
* LVC.full: MWE-based: P=50/74=0.6757 R=50/121=0.4132 F=0.5128
* LVC.full: Tok-based: P=116/163=0.7117 R=116/266=0.4361 F=0.5408
* VID: MWE-proportion: gold=54/228=24% pred=27/164=16%
* VID: MWE-based: P=19/27=0.7037 R=19/54=0.3519 F=0.4691
* VID: Tok-based: P=40/58=0.6897 R=40/123=0.3252 F=0.4420

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=103/164=63%
* Continuous: MWE-based: P=69/103=0.6699 R=69/126=0.5476 F=0.6026
* Discontinuous: MWE-proportion: gold=102/228=45% pred=61/164=37%
* Discontinuous: MWE-based: P=37/61=0.6066 R=37/102=0.3627 F=0.4540

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=164/164=100%
* Multi-token: MWE-based: P=106/164=0.6463 R=106/227=0.4670 F=0.5422
* Single-token: MWE-proportion: gold=1/228=0% pred=0/164=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=164/164=100%
* Seen-in-train: MWE-based: P=106/164=0.6463 R=106/128=0.8281 F=0.7260
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/164=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=93/164=57%
* Variant-of-train: MWE-based: P=53/93=0.5699 R=53/74=0.7162 F=0.6347
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=71/164=43%
* Identical-to-train: MWE-based: P=53/71=0.7465 R=53/54=0.9815 F=0.8480

