## Global evaluation
* MWE-based: P=81/120=0.6750 R=81/228=0.3553 F=0.4655
* Tok-based: P=181/261=0.6935 R=181/498=0.3635 F=0.4769

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=4/120=3%
* IAV: MWE-based: P=0/4=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=1/8=0.1250 R=1/76=0.0132 F=0.0238
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/120=7%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=13/17=0.7647 R=13/33=0.3939 F=0.5200
* LVC.full: MWE-proportion: gold=121/228=53% pred=69/120=57%
* LVC.full: MWE-based: P=46/69=0.6667 R=46/121=0.3802 F=0.4842
* LVC.full: Tok-based: P=104/153=0.6797 R=104/266=0.3910 F=0.4964
* MVC: MWE-proportion: gold=0/228=0% pred=1/120=1%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=38/120=32%
* VID: MWE-based: P=24/38=0.6316 R=24/54=0.4444 F=0.5217
* VID: Tok-based: P=52/81=0.6420 R=52/123=0.4228 F=0.5098

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=64/120=53%
* Continuous: MWE-based: P=49/64=0.7656 R=49/126=0.3889 F=0.5158
* Discontinuous: MWE-proportion: gold=102/228=45% pred=56/120=47%
* Discontinuous: MWE-based: P=32/56=0.5714 R=32/102=0.3137 F=0.4051

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=120/120=100%
* Multi-token: MWE-based: P=81/120=0.6750 R=81/227=0.3568 F=0.4669
* Single-token: MWE-proportion: gold=1/228=0% pred=0/120=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=120/120=100%
* Seen-in-train: MWE-based: P=81/120=0.6750 R=81/128=0.6328 F=0.6532
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/120=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=75/120=62%
* Variant-of-train: MWE-based: P=48/75=0.6400 R=48/74=0.6486 F=0.6443
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=45/120=38%
* Identical-to-train: MWE-based: P=33/45=0.7333 R=33/54=0.6111 F=0.6667

