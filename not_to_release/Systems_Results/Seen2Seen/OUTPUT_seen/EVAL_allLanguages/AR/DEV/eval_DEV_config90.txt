## Global evaluation
* MWE-based: P=74/95=0.7789 R=74/228=0.3246 F=0.4582
* Tok-based: P=163/205=0.7951 R=163/498=0.3273 F=0.4637

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=0/95=0%
* IAV: MWE-based: P=0/0=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=0/0=0.0000 R=0/76=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=15/228=7% pred=7/95=7%
* LVC.cause: MWE-based: P=5/7=0.7143 R=5/15=0.3333 F=0.4545
* LVC.cause: Tok-based: P=10/14=0.7143 R=10/33=0.3030 F=0.4255
* LVC.full: MWE-proportion: gold=121/228=53% pred=64/95=67%
* LVC.full: MWE-based: P=46/64=0.7188 R=46/121=0.3802 F=0.4973
* LVC.full: Tok-based: P=104/141=0.7376 R=104/266=0.3910 F=0.5111
* VID: MWE-proportion: gold=54/228=24% pred=24/95=25%
* VID: MWE-based: P=18/24=0.7500 R=18/54=0.3333 F=0.4615
* VID: Tok-based: P=38/50=0.7600 R=38/123=0.3089 F=0.4393

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=63/95=66%
* Continuous: MWE-based: P=50/63=0.7937 R=50/126=0.3968 F=0.5291
* Discontinuous: MWE-proportion: gold=102/228=45% pred=32/95=34%
* Discontinuous: MWE-based: P=24/32=0.7500 R=24/102=0.2353 F=0.3582

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=95/95=100%
* Multi-token: MWE-based: P=74/95=0.7789 R=74/227=0.3260 F=0.4596
* Single-token: MWE-proportion: gold=1/228=0% pred=0/95=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=95/95=100%
* Seen-in-train: MWE-based: P=74/95=0.7789 R=74/128=0.5781 F=0.6637
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/95=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=52/95=55%
* Variant-of-train: MWE-based: P=41/52=0.7885 R=41/74=0.5541 F=0.6508
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=43/95=45%
* Identical-to-train: MWE-based: P=33/43=0.7674 R=33/54=0.6111 F=0.6804

