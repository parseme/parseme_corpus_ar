## Global evaluation
* MWE-based: P=116/275=0.4218 R=116/228=0.5088 F=0.4612
* Tok-based: P=280/591=0.4738 R=280/498=0.5622 F=0.5142

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=101/275=37%
* IAV: MWE-based: P=31/101=0.3069 R=31/38=0.8158 F=0.4460
* IAV: Tok-based: P=62/202=0.3069 R=62/76=0.8158 F=0.4460
* LVC.cause: MWE-proportion: gold=15/228=7% pred=12/275=4%
* LVC.cause: MWE-based: P=7/12=0.5833 R=7/15=0.4667 F=0.5185
* LVC.cause: Tok-based: P=15/26=0.5769 R=15/33=0.4545 F=0.5085
* LVC.full: MWE-proportion: gold=121/228=53% pred=112/275=41%
* LVC.full: MWE-based: P=51/112=0.4554 R=51/121=0.4215 F=0.4378
* LVC.full: Tok-based: P=133/255=0.5216 R=133/266=0.5000 F=0.5106
* MVC: MWE-proportion: gold=0/228=0% pred=1/275=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=49/275=18%
* VID: MWE-based: P=22/49=0.4490 R=22/54=0.4074 F=0.4272
* VID: Tok-based: P=52/106=0.4906 R=52/123=0.4228 F=0.4541

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=98/275=36%
* Continuous: MWE-based: P=64/98=0.6531 R=64/126=0.5079 F=0.5714
* Discontinuous: MWE-proportion: gold=102/228=45% pred=177/275=64%
* Discontinuous: MWE-based: P=52/177=0.2938 R=52/102=0.5098 F=0.3728

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=275/275=100%
* Multi-token: MWE-based: P=116/275=0.4218 R=116/227=0.5110 F=0.4622
* Single-token: MWE-proportion: gold=1/228=0% pred=0/275=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=275/275=100%
* Seen-in-train: MWE-based: P=116/275=0.4218 R=116/128=0.9062 F=0.5757
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/275=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=207/275=75%
* Variant-of-train: MWE-based: P=66/207=0.3188 R=66/74=0.8919 F=0.4698
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=68/275=25%
* Identical-to-train: MWE-based: P=50/68=0.7353 R=50/54=0.9259 F=0.8197

