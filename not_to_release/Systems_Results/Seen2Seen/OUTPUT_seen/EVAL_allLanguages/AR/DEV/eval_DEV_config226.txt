## Global evaluation
* MWE-based: P=81/128=0.6328 R=81/228=0.3553 F=0.4551
* Tok-based: P=182/277=0.6570 R=182/498=0.3655 F=0.4697

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=3/128=2%
* IAV: MWE-based: P=0/3=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=1/6=0.1667 R=1/76=0.0132 F=0.0244
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/128=6%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=13/17=0.7647 R=13/33=0.3939 F=0.5200
* LVC.full: MWE-proportion: gold=121/228=53% pred=81/128=63%
* LVC.full: MWE-based: P=49/81=0.6049 R=49/121=0.4050 F=0.4851
* LVC.full: Tok-based: P=110/177=0.6215 R=110/266=0.4135 F=0.4966
* MVC: MWE-proportion: gold=0/228=0% pred=1/128=1%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=35/128=27%
* VID: MWE-based: P=21/35=0.6000 R=21/54=0.3889 F=0.4719
* VID: Tok-based: P=47/75=0.6267 R=47/123=0.3821 F=0.4747

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=65/128=51%
* Continuous: MWE-based: P=49/65=0.7538 R=49/126=0.3889 F=0.5131
* Discontinuous: MWE-proportion: gold=102/228=45% pred=63/128=49%
* Discontinuous: MWE-based: P=32/63=0.5079 R=32/102=0.3137 F=0.3879

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=128/128=100%
* Multi-token: MWE-based: P=81/128=0.6328 R=81/227=0.3568 F=0.4563
* Single-token: MWE-proportion: gold=1/228=0% pred=0/128=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=128/128=100%
* Seen-in-train: MWE-based: P=81/128=0.6328 R=81/128=0.6328 F=0.6328
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/128=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=86/128=67%
* Variant-of-train: MWE-based: P=49/86=0.5698 R=49/74=0.6622 F=0.6125
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=42/128=33%
* Identical-to-train: MWE-based: P=32/42=0.7619 R=32/54=0.5926 F=0.6667

