## Global evaluation
* MWE-based: P=102/156=0.6538 R=102/228=0.4474 F=0.5312
* Tok-based: P=227/329=0.6900 R=227/498=0.4558 F=0.5490

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=57/156=37%
* IAV: MWE-based: P=27/57=0.4737 R=27/38=0.7105 F=0.5684
* IAV: Tok-based: P=55/114=0.4825 R=55/76=0.7237 F=0.5789
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/156=5%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=12/16=0.7500 R=12/33=0.3636 F=0.4898
* LVC.full: MWE-proportion: gold=121/228=53% pred=66/156=42%
* LVC.full: MWE-based: P=45/66=0.6818 R=45/121=0.3719 F=0.4813
* LVC.full: Tok-based: P=105/146=0.7192 R=105/266=0.3947 F=0.5097
* VID: MWE-proportion: gold=54/228=24% pred=25/156=16%
* VID: MWE-based: P=19/25=0.7600 R=19/54=0.3519 F=0.4810
* VID: Tok-based: P=40/53=0.7547 R=40/123=0.3252 F=0.4545

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=101/156=65%
* Continuous: MWE-based: P=68/101=0.6733 R=68/126=0.5397 F=0.5991
* Discontinuous: MWE-proportion: gold=102/228=45% pred=55/156=35%
* Discontinuous: MWE-based: P=34/55=0.6182 R=34/102=0.3333 F=0.4331

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=156/156=100%
* Multi-token: MWE-based: P=102/156=0.6538 R=102/227=0.4493 F=0.5326
* Single-token: MWE-proportion: gold=1/228=0% pred=0/156=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=156/156=100%
* Seen-in-train: MWE-based: P=102/156=0.6538 R=102/128=0.7969 F=0.7183
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/156=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=84/156=54%
* Variant-of-train: MWE-based: P=48/84=0.5714 R=48/74=0.6486 F=0.6076
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=72/156=46%
* Identical-to-train: MWE-based: P=54/72=0.7500 R=54/54=1.0000 F=0.8571

