## Global evaluation
* MWE-based: P=81/129=0.6279 R=81/228=0.3553 F=0.4538
* Tok-based: P=182/279=0.6523 R=182/498=0.3655 F=0.4685

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=3/129=2%
* IAV: MWE-based: P=0/3=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=1/6=0.1667 R=1/76=0.0132 F=0.0244
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/129=6%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=13/17=0.7647 R=13/33=0.3939 F=0.5200
* LVC.full: MWE-proportion: gold=121/228=53% pred=81/129=63%
* LVC.full: MWE-based: P=49/81=0.6049 R=49/121=0.4050 F=0.4851
* LVC.full: Tok-based: P=110/177=0.6215 R=110/266=0.4135 F=0.4966
* MVC: MWE-proportion: gold=0/228=0% pred=1/129=1%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=36/129=28%
* VID: MWE-based: P=21/36=0.5833 R=21/54=0.3889 F=0.4667
* VID: Tok-based: P=47/77=0.6104 R=47/123=0.3821 F=0.4700

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=64/129=50%
* Continuous: MWE-based: P=49/64=0.7656 R=49/126=0.3889 F=0.5158
* Discontinuous: MWE-proportion: gold=102/228=45% pred=65/129=50%
* Discontinuous: MWE-based: P=32/65=0.4923 R=32/102=0.3137 F=0.3832

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=129/129=100%
* Multi-token: MWE-based: P=81/129=0.6279 R=81/227=0.3568 F=0.4551
* Single-token: MWE-proportion: gold=1/228=0% pred=0/129=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=129/129=100%
* Seen-in-train: MWE-based: P=81/129=0.6279 R=81/128=0.6328 F=0.6304
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/129=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=85/129=66%
* Variant-of-train: MWE-based: P=49/85=0.5765 R=49/74=0.6622 F=0.6164
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=44/129=34%
* Identical-to-train: MWE-based: P=32/44=0.7273 R=32/54=0.5926 F=0.6531

