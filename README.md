README
------
This is the README file for the PARSEME verbal multiword expressions (VMWEs) corpus for Arabic, edition 1.3. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

Source corpora
-------
All the source data (columns 1-10, see below) come from The [Universal Dependencies Arabic PADT corpus](https://github.com/UniversalDependencies/UD_Arabic-PADT/tree/master), version 2.8 (as of May 2021).

Format
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format, which is an instantiation of the [CoNLL-U Plus format](https://universaldependencies.org/ext-format.html).
The following tagsets are used:
* column 3 (Lemmas): Annotated manually in non-UD style, automatically converted to UD.
* column 4 (UPOS): Converted automatically from XPOSTAG (via Interset); human checking of patterns revealed by automatic consistency tests.[UD POS-tags](http://universaldependencies.org/u/pos) version 2 (as of May 2021).
* column 5 (XPOS): Manual selection from possibilities provided by [ElixirFM](http://elixir-fm.sf.net/) tagset.
* column 6 (FEATS): Converted automatically from XPOSTAG (via Interset); human checking of patterns revealed by automatic consistency tests. [UD features](http://universaldependencies.org/u/feat/index.html) version 2 (as of May 2021).
* column 8 (DEPREL): Original PDT annotation is manual. Automatic conversion to UD; human checking of patterns revealed by automatic consistency tests. [UD dependency relations](http://universaldependencies.org/u/dep) version 2 (as of March 2021).
* column 10 (MISC): Contains additional word attributes provided by ElixirFM rules and manual disambiguation: Vform (fully vocalized Arabic form), Translit (Latin transliteration of the word form), LTranslit (Latin transliteration of the lemma), Root (word root), Gloss (English translation of the lemma).
* column 11 (PARSEME:MWE): Manually annotated by a single annotator per file. [PARSEME VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.2/?page=030_Categories_of_VMWEs) version 1.2.


The Arabic-PADT UD treebank is based on the Prague Arabic Dependency Treebank (PADT), created at the Charles University in Prague, release 1.2.

The treebank consists of 7,664 sentences (282,384 tokens) and its domain is mainly newswire. The annotation is licensed under the terms of CC BY-NC-SA 3.0 and its original (non-UD) version can be downloaded from http://hdl.handle.net/11858/00-097C-0000-0001-4872-3.

The morphological and syntactic annotation of the Arabic UD treebank is created through conversion of PADT data. The conversion procedure has been designed by Dan Zeman. The main coordinator of the original PADT project was Otakar Smrž.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

Text genres and origins
---------------------------
The texts stem from Arabic newspapers namely the Agence France Press, the Al Hayat News Agency, and the Xinhua News Agency.

Authors
----------
All VMWEs annotations (column 11) were performed by Najet Hadj Mohamed and Cherifa Ben Khelil. For authorship of the data in columns 1-10 see the original [PADT corpus](https://github.com/UniversalDependencies/UD_Arabic-PADT/tree/master).

License
----------
The VMWEs annotations (column 11) are distributed under the terms of the [CC-BY v4](https://creativecommons.org/licenses/by/4.0/) license.
The remaining data (columns 1-10), stemming from the PADT source corpus, are distributed under the terms of the [BY-NC-SA v3](http://creativecommons.org/licenses/by-nc-sa/3.0/) license.

Contact
----------

* hadjmed.dhouha@gmail.com
* cherifa.bk@gmail.com

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.

