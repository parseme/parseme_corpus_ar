## Global evaluation
* MWE-based: P=78/111=0.7027 R=78/228=0.3421 F=0.4602
* Tok-based: P=176/243=0.7243 R=176/498=0.3534 F=0.4750

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=1/111=1%
* IAV: MWE-based: P=0/1=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=1/2=0.5000 R=1/76=0.0132 F=0.0256
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/111=7%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=13/17=0.7647 R=13/33=0.3939 F=0.5200
* LVC.full: MWE-proportion: gold=121/228=53% pred=67/111=60%
* LVC.full: MWE-based: P=46/67=0.6866 R=46/121=0.3802 F=0.4894
* LVC.full: Tok-based: P=104/149=0.6980 R=104/266=0.3910 F=0.5012
* MVC: MWE-proportion: gold=0/228=0% pred=1/111=1%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=34/111=31%
* VID: MWE-based: P=21/34=0.6176 R=21/54=0.3889 F=0.4773
* VID: Tok-based: P=47/73=0.6438 R=47/123=0.3821 F=0.4796

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=62/111=56%
* Continuous: MWE-based: P=48/62=0.7742 R=48/126=0.3810 F=0.5106
* Discontinuous: MWE-proportion: gold=102/228=45% pred=49/111=44%
* Discontinuous: MWE-based: P=30/49=0.6122 R=30/102=0.2941 F=0.3974

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=111/111=100%
* Multi-token: MWE-based: P=78/111=0.7027 R=78/227=0.3436 F=0.4615
* Single-token: MWE-proportion: gold=1/228=0% pred=0/111=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=111/111=100%
* Seen-in-train: MWE-based: P=78/111=0.7027 R=78/128=0.6094 F=0.6527
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/111=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=69/111=62%
* Variant-of-train: MWE-based: P=46/69=0.6667 R=46/74=0.6216 F=0.6434
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=42/111=38%
* Identical-to-train: MWE-based: P=32/42=0.7619 R=32/54=0.5926 F=0.6667

