## Global evaluation
* MWE-based: P=74/98=0.7551 R=74/228=0.3246 F=0.4540
* Tok-based: P=163/211=0.7725 R=163/498=0.3273 F=0.4598

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=0/98=0%
* IAV: MWE-based: P=0/0=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=0/0=0.0000 R=0/76=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=15/228=7% pred=7/98=7%
* LVC.cause: MWE-based: P=5/7=0.7143 R=5/15=0.3333 F=0.4545
* LVC.cause: Tok-based: P=10/14=0.7143 R=10/33=0.3030 F=0.4255
* LVC.full: MWE-proportion: gold=121/228=53% pred=66/98=67%
* LVC.full: MWE-based: P=46/66=0.6970 R=46/121=0.3802 F=0.4920
* LVC.full: Tok-based: P=104/145=0.7172 R=104/266=0.3910 F=0.5061
* VID: MWE-proportion: gold=54/228=24% pred=25/98=26%
* VID: MWE-based: P=18/25=0.7200 R=18/54=0.3333 F=0.4557
* VID: Tok-based: P=38/52=0.7308 R=38/123=0.3089 F=0.4343

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=63/98=64%
* Continuous: MWE-based: P=50/63=0.7937 R=50/126=0.3968 F=0.5291
* Discontinuous: MWE-proportion: gold=102/228=45% pred=35/98=36%
* Discontinuous: MWE-based: P=24/35=0.6857 R=24/102=0.2353 F=0.3504

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=98/98=100%
* Multi-token: MWE-based: P=74/98=0.7551 R=74/227=0.3260 F=0.4554
* Single-token: MWE-proportion: gold=1/228=0% pred=0/98=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=98/98=100%
* Seen-in-train: MWE-based: P=74/98=0.7551 R=74/128=0.5781 F=0.6549
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/98=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=53/98=54%
* Variant-of-train: MWE-based: P=41/53=0.7736 R=41/74=0.5541 F=0.6457
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=45/98=46%
* Identical-to-train: MWE-based: P=33/45=0.7333 R=33/54=0.6111 F=0.6667

