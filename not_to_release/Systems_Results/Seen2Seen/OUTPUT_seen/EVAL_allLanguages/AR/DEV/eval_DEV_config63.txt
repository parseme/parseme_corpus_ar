## Global evaluation
* MWE-based: P=128/549=0.2332 R=128/228=0.5614 F=0.3295
* Tok-based: P=310/1232=0.2516 R=310/498=0.6225 F=0.3584

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=166/549=30%
* IAV: MWE-based: P=33/166=0.1988 R=33/38=0.8684 F=0.3235
* IAV: Tok-based: P=66/332=0.1988 R=66/76=0.8684 F=0.3235
* LVC.cause: MWE-proportion: gold=15/228=7% pred=15/549=3%
* LVC.cause: MWE-based: P=7/15=0.4667 R=7/15=0.4667 F=0.4667
* LVC.cause: Tok-based: P=15/33=0.4545 R=15/33=0.4545 F=0.4545
* LVC.full: MWE-proportion: gold=121/228=53% pred=259/549=47%
* LVC.full: MWE-based: P=57/259=0.2201 R=57/121=0.4711 F=0.3000
* LVC.full: Tok-based: P=155/613=0.2529 R=155/266=0.5827 F=0.3527
* MVC: MWE-proportion: gold=0/228=0% pred=1/549=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=108/549=20%
* VID: MWE-based: P=26/108=0.2407 R=26/54=0.4815 F=0.3210
* VID: Tok-based: P=58/252=0.2302 R=58/123=0.4715 F=0.3093

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=105/549=19%
* Continuous: MWE-based: P=69/105=0.6571 R=69/126=0.5476 F=0.5974
* Discontinuous: MWE-proportion: gold=102/228=45% pred=444/549=81%
* Discontinuous: MWE-based: P=59/444=0.1329 R=59/102=0.5784 F=0.2161

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=549/549=100%
* Multi-token: MWE-based: P=128/549=0.2332 R=128/227=0.5639 F=0.3299
* Single-token: MWE-proportion: gold=1/228=0% pred=0/549=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=549/549=100%
* Seen-in-train: MWE-based: P=128/549=0.2332 R=128/128=1.0000 F=0.3781
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/549=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=474/549=86%
* Variant-of-train: MWE-based: P=74/474=0.1561 R=74/74=1.0000 F=0.2701
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=75/549=14%
* Identical-to-train: MWE-based: P=54/75=0.7200 R=54/54=1.0000 F=0.8372

