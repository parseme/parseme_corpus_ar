## Global evaluation
* MWE-based: P=308/528=0.5833 R=308/680=0.4529 F=0.5099
* Tok-based: P=712/1125=0.6329 R=712/1554=0.4582 F=0.5315

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=73/680=11% pred=150/528=28%
* IAV: MWE-based: P=49/150=0.3267 R=49/73=0.6712 F=0.4395
* IAV: Tok-based: P=98/300=0.3267 R=98/146=0.6712 F=0.4395
* LVC.cause: MWE-proportion: gold=52/680=8% pred=19/528=4%
* LVC.cause: MWE-based: P=12/19=0.6316 R=12/52=0.2308 F=0.3380
* LVC.cause: Tok-based: P=30/42=0.7143 R=30/121=0.2479 F=0.3681
* LVC.full: MWE-proportion: gold=379/680=56% pred=254/528=48%
* LVC.full: MWE-based: P=157/254=0.6181 R=157/379=0.4142 F=0.4961
* LVC.full: Tok-based: P=373/550=0.6782 R=373/864=0.4317 F=0.5276
* MVC: MWE-proportion: gold=1/680=0% pred=0/528=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000
* VID: MWE-proportion: gold=173/680=25% pred=105/528=20%
* VID: MWE-based: P=75/105=0.7143 R=75/173=0.4335 F=0.5396
* VID: Tok-based: P=166/233=0.7124 R=166/417=0.3981 F=0.5108
* VPC.full: MWE-proportion: gold=2/680=0% pred=0/528=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/4=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=390/680=57% pred=334/528=63%
* Continuous: MWE-based: P=202/334=0.6048 R=202/390=0.5179 F=0.5580
* Discontinuous: MWE-proportion: gold=290/680=43% pred=194/528=37%
* Discontinuous: MWE-based: P=106/194=0.5464 R=106/290=0.3655 F=0.4380

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=680/680=100% pred=528/528=100%
* Multi-token: MWE-based: P=308/528=0.5833 R=308/680=0.4529 F=0.5099
* Single-token: MWE-proportion: gold=0/680=0% pred=0/528=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/0=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=380/680=56% pred=528/528=100%
* Seen-in-train: MWE-based: P=308/528=0.5833 R=308/380=0.8105 F=0.6784
* Unseen-in-train: MWE-proportion: gold=300/680=44% pred=0/528=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/300=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=234/380=62% pred=330/528=62%
* Variant-of-train: MWE-based: P=165/330=0.5000 R=165/234=0.7051 F=0.5851
* Identical-to-train: MWE-proportion: gold=146/380=38% pred=198/528=38%
* Identical-to-train: MWE-based: P=143/198=0.7222 R=143/146=0.9795 F=0.8314

