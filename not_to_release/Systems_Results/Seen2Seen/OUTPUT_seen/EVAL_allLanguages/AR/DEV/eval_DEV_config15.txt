## Global evaluation
* MWE-based: P=106/169=0.6272 R=106/228=0.4649 F=0.5340
* Tok-based: P=236/357=0.6611 R=236/498=0.4739 F=0.5520

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=57/169=34%
* IAV: MWE-based: P=27/57=0.4737 R=27/38=0.7105 F=0.5684
* IAV: Tok-based: P=55/114=0.4825 R=55/76=0.7237 F=0.5789
* LVC.cause: MWE-proportion: gold=15/228=7% pred=7/169=4%
* LVC.cause: MWE-based: P=5/7=0.7143 R=5/15=0.3333 F=0.4545
* LVC.cause: Tok-based: P=10/14=0.7143 R=10/33=0.3030 F=0.4255
* LVC.full: MWE-proportion: gold=121/228=53% pred=76/169=45%
* LVC.full: MWE-based: P=50/76=0.6579 R=50/121=0.4132 F=0.5076
* LVC.full: Tok-based: P=116/167=0.6946 R=116/266=0.4361 F=0.5358
* VID: MWE-proportion: gold=54/228=24% pred=29/169=17%
* VID: MWE-based: P=19/29=0.6552 R=19/54=0.3519 F=0.4578
* VID: Tok-based: P=40/62=0.6452 R=40/123=0.3252 F=0.4324

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=104/169=62%
* Continuous: MWE-based: P=69/104=0.6635 R=69/126=0.5476 F=0.6000
* Discontinuous: MWE-proportion: gold=102/228=45% pred=65/169=38%
* Discontinuous: MWE-based: P=37/65=0.5692 R=37/102=0.3627 F=0.4431

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=169/169=100%
* Multi-token: MWE-based: P=106/169=0.6272 R=106/227=0.4670 F=0.5354
* Single-token: MWE-proportion: gold=1/228=0% pred=0/169=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=169/169=100%
* Seen-in-train: MWE-based: P=106/169=0.6272 R=106/128=0.8281 F=0.7138
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/169=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=95/169=56%
* Variant-of-train: MWE-based: P=53/95=0.5579 R=53/74=0.7162 F=0.6272
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=74/169=44%
* Identical-to-train: MWE-based: P=53/74=0.7162 R=53/54=0.9815 F=0.8281

