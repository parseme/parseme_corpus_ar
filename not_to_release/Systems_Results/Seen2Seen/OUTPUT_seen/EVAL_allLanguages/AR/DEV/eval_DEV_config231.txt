## Global evaluation
* MWE-based: P=122/320=0.3812 R=122/228=0.5351 F=0.4453
* Tok-based: P=293/687=0.4265 R=293/498=0.5884 F=0.4945

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=110/320=34%
* IAV: MWE-based: P=31/110=0.2818 R=31/38=0.8158 F=0.4189
* IAV: Tok-based: P=63/220=0.2864 R=63/76=0.8289 F=0.4257
* LVC.cause: MWE-proportion: gold=15/228=7% pred=10/320=3%
* LVC.cause: MWE-based: P=6/10=0.6000 R=6/15=0.4000 F=0.4800
* LVC.cause: Tok-based: P=13/22=0.5909 R=13/33=0.3939 F=0.4727
* LVC.full: MWE-proportion: gold=121/228=53% pred=140/320=44%
* LVC.full: MWE-based: P=57/140=0.4071 R=57/121=0.4711 F=0.4368
* LVC.full: Tok-based: P=147/311=0.4727 R=147/266=0.5526 F=0.5095
* MVC: MWE-proportion: gold=0/228=0% pred=1/320=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=59/320=18%
* VID: MWE-based: P=23/59=0.3898 R=23/54=0.4259 F=0.4071
* VID: Tok-based: P=56/132=0.4242 R=56/123=0.4553 F=0.4392

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=108/320=34%
* Continuous: MWE-based: P=67/108=0.6204 R=67/126=0.5317 F=0.5726
* Discontinuous: MWE-proportion: gold=102/228=45% pred=212/320=66%
* Discontinuous: MWE-based: P=55/212=0.2594 R=55/102=0.5392 F=0.3503

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=320/320=100%
* Multi-token: MWE-based: P=122/320=0.3812 R=122/227=0.5374 F=0.4461
* Single-token: MWE-proportion: gold=1/228=0% pred=0/320=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=320/320=100%
* Seen-in-train: MWE-based: P=122/320=0.3812 R=122/128=0.9531 F=0.5446
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/320=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=246/320=77%
* Variant-of-train: MWE-based: P=71/246=0.2886 R=71/74=0.9595 F=0.4438
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=74/320=23%
* Identical-to-train: MWE-based: P=51/74=0.6892 R=51/54=0.9444 F=0.7969

