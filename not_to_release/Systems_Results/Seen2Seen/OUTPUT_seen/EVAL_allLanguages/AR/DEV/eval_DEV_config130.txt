## Global evaluation
* MWE-based: P=73/94=0.7766 R=73/228=0.3202 F=0.4534
* Tok-based: P=161/203=0.7931 R=161/498=0.3233 F=0.4593

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=0/94=0%
* IAV: MWE-based: P=0/0=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=0/0=0.0000 R=0/76=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=15/228=7% pred=7/94=7%
* LVC.cause: MWE-based: P=5/7=0.7143 R=5/15=0.3333 F=0.4545
* LVC.cause: Tok-based: P=10/14=0.7143 R=10/33=0.3030 F=0.4255
* LVC.full: MWE-proportion: gold=121/228=53% pred=64/94=68%
* LVC.full: MWE-based: P=46/64=0.7188 R=46/121=0.3802 F=0.4973
* LVC.full: Tok-based: P=104/141=0.7376 R=104/266=0.3910 F=0.5111
* VID: MWE-proportion: gold=54/228=24% pred=23/94=24%
* VID: MWE-based: P=17/23=0.7391 R=17/54=0.3148 F=0.4416
* VID: Tok-based: P=36/48=0.7500 R=36/123=0.2927 F=0.4211

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=62/94=66%
* Continuous: MWE-based: P=49/62=0.7903 R=49/126=0.3889 F=0.5213
* Discontinuous: MWE-proportion: gold=102/228=45% pred=32/94=34%
* Discontinuous: MWE-based: P=24/32=0.7500 R=24/102=0.2353 F=0.3582

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=94/94=100%
* Multi-token: MWE-based: P=73/94=0.7766 R=73/227=0.3216 F=0.4548
* Single-token: MWE-proportion: gold=1/228=0% pred=0/94=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=94/94=100%
* Seen-in-train: MWE-based: P=73/94=0.7766 R=73/128=0.5703 F=0.6577
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/94=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=52/94=55%
* Variant-of-train: MWE-based: P=41/52=0.7885 R=41/74=0.5541 F=0.6508
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=42/94=45%
* Identical-to-train: MWE-based: P=32/42=0.7619 R=32/54=0.5926 F=0.6667

