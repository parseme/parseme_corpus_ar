## Global evaluation
* MWE-based: P=119/456=0.2610 R=119/228=0.5219 F=0.3480
* Tok-based: P=292/1030=0.2835 R=292/498=0.5863 F=0.3822

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=164/456=36%
* IAV: MWE-based: P=32/164=0.1951 R=32/38=0.8421 F=0.3168
* IAV: Tok-based: P=64/328=0.1951 R=64/76=0.8421 F=0.3168
* LVC.cause: MWE-proportion: gold=15/228=7% pred=15/456=3%
* LVC.cause: MWE-based: P=7/15=0.4667 R=7/15=0.4667 F=0.4667
* LVC.cause: Tok-based: P=15/33=0.4545 R=15/33=0.4545 F=0.4545
* LVC.full: MWE-proportion: gold=121/228=53% pred=190/456=42%
* LVC.full: MWE-based: P=51/190=0.2684 R=51/121=0.4215 F=0.3280
* LVC.full: Tok-based: P=138/462=0.2987 R=138/266=0.5188 F=0.3791
* MVC: MWE-proportion: gold=0/228=0% pred=1/456=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=86/456=19%
* VID: MWE-based: P=24/86=0.2791 R=24/54=0.4444 F=0.3429
* VID: Tok-based: P=56/205=0.2732 R=56/123=0.4553 F=0.3415

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=99/456=22%
* Continuous: MWE-based: P=65/99=0.6566 R=65/126=0.5159 F=0.5778
* Discontinuous: MWE-proportion: gold=102/228=45% pred=357/456=78%
* Discontinuous: MWE-based: P=54/357=0.1513 R=54/102=0.5294 F=0.2353

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=456/456=100%
* Multi-token: MWE-based: P=119/456=0.2610 R=119/227=0.5242 F=0.3485
* Single-token: MWE-proportion: gold=1/228=0% pred=0/456=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=456/456=100%
* Seen-in-train: MWE-based: P=119/456=0.2610 R=119/128=0.9297 F=0.4075
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/456=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=387/456=85%
* Variant-of-train: MWE-based: P=68/387=0.1757 R=68/74=0.9189 F=0.2950
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=69/456=15%
* Identical-to-train: MWE-based: P=51/69=0.7391 R=51/54=0.9444 F=0.8293

