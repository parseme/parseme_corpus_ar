## Global evaluation
* MWE-based: P=119/589=0.2020 R=119/228=0.5219 F=0.2913
* Tok-based: P=296/1336=0.2216 R=296/498=0.5944 F=0.3228

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=263/589=45%
* IAV: MWE-based: P=32/263=0.1217 R=32/38=0.8421 F=0.2126
* IAV: Tok-based: P=64/526=0.1217 R=64/76=0.8421 F=0.2126
* LVC.cause: MWE-proportion: gold=15/228=7% pred=15/589=3%
* LVC.cause: MWE-based: P=7/15=0.4667 R=7/15=0.4667 F=0.4667
* LVC.cause: Tok-based: P=15/33=0.4545 R=15/33=0.4545 F=0.4545
* LVC.full: MWE-proportion: gold=121/228=53% pred=205/589=35%
* LVC.full: MWE-based: P=51/205=0.2488 R=51/121=0.4215 F=0.3129
* LVC.full: Tok-based: P=138/506=0.2727 R=138/266=0.5188 F=0.3575
* MVC: MWE-proportion: gold=0/228=0% pred=1/589=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=105/589=18%
* VID: MWE-based: P=24/105=0.2286 R=24/54=0.4444 F=0.3019
* VID: Tok-based: P=60/269=0.2230 R=60/123=0.4878 F=0.3061

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=103/589=17%
* Continuous: MWE-based: P=65/103=0.6311 R=65/126=0.5159 F=0.5677
* Discontinuous: MWE-proportion: gold=102/228=45% pred=486/589=83%
* Discontinuous: MWE-based: P=54/486=0.1111 R=54/102=0.5294 F=0.1837

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=589/589=100%
* Multi-token: MWE-based: P=119/589=0.2020 R=119/227=0.5242 F=0.2917
* Single-token: MWE-proportion: gold=1/228=0% pred=0/589=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=589/589=100%
* Seen-in-train: MWE-based: P=119/589=0.2020 R=119/128=0.9297 F=0.3319
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/589=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=518/589=88%
* Variant-of-train: MWE-based: P=68/518=0.1313 R=68/74=0.9189 F=0.2297
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=71/589=12%
* Identical-to-train: MWE-based: P=51/71=0.7183 R=51/54=0.9444 F=0.8160

