## Global evaluation
* MWE-based: P=102/160=0.6375 R=102/228=0.4474 F=0.5258
* Tok-based: P=227/337=0.6736 R=227/498=0.4558 F=0.5437

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=57/160=36%
* IAV: MWE-based: P=27/57=0.4737 R=27/38=0.7105 F=0.5684
* IAV: Tok-based: P=55/114=0.4825 R=55/76=0.7237 F=0.5789
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/160=5%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=12/16=0.7500 R=12/33=0.3636 F=0.4898
* LVC.full: MWE-proportion: gold=121/228=53% pred=68/160=42%
* LVC.full: MWE-based: P=45/68=0.6618 R=45/121=0.3719 F=0.4762
* LVC.full: Tok-based: P=105/150=0.7000 R=105/266=0.3947 F=0.5048
* VID: MWE-proportion: gold=54/228=24% pred=27/160=17%
* VID: MWE-based: P=19/27=0.7037 R=19/54=0.3519 F=0.4691
* VID: Tok-based: P=40/57=0.7018 R=40/123=0.3252 F=0.4444

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=102/160=64%
* Continuous: MWE-based: P=68/102=0.6667 R=68/126=0.5397 F=0.5965
* Discontinuous: MWE-proportion: gold=102/228=45% pred=58/160=36%
* Discontinuous: MWE-based: P=34/58=0.5862 R=34/102=0.3333 F=0.4250

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=160/160=100%
* Multi-token: MWE-based: P=102/160=0.6375 R=102/227=0.4493 F=0.5271
* Single-token: MWE-proportion: gold=1/228=0% pred=0/160=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=160/160=100%
* Seen-in-train: MWE-based: P=102/160=0.6375 R=102/128=0.7969 F=0.7083
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/160=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=85/160=53%
* Variant-of-train: MWE-based: P=48/85=0.5647 R=48/74=0.6486 F=0.6038
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=75/160=47%
* Identical-to-train: MWE-based: P=54/75=0.7200 R=54/54=1.0000 F=0.8372

