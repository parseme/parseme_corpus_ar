## Global evaluation
* MWE-based: P=117/314=0.3726 R=117/228=0.5132 F=0.4317
* Tok-based: P=282/696=0.4052 R=282/498=0.5663 F=0.4724

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=106/314=34%
* IAV: MWE-based: P=31/106=0.2925 R=31/38=0.8158 F=0.4306
* IAV: Tok-based: P=62/212=0.2925 R=62/76=0.8158 F=0.4306
* LVC.cause: MWE-proportion: gold=15/228=7% pred=11/314=4%
* LVC.cause: MWE-based: P=6/11=0.5455 R=6/15=0.4000 F=0.4615
* LVC.cause: Tok-based: P=13/24=0.5417 R=13/33=0.3939 F=0.4561
* LVC.full: MWE-proportion: gold=121/228=53% pred=140/314=45%
* LVC.full: MWE-based: P=51/140=0.3643 R=51/121=0.4215 F=0.3908
* LVC.full: Tok-based: P=134/333=0.4024 R=134/266=0.5038 F=0.4474
* MVC: MWE-proportion: gold=0/228=0% pred=1/314=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=56/314=18%
* VID: MWE-based: P=24/56=0.4286 R=24/54=0.4444 F=0.4364
* VID: Tok-based: P=56/125=0.4480 R=56/123=0.4553 F=0.4516

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=102/314=32%
* Continuous: MWE-based: P=65/102=0.6373 R=65/126=0.5159 F=0.5702
* Discontinuous: MWE-proportion: gold=102/228=45% pred=212/314=68%
* Discontinuous: MWE-based: P=52/212=0.2453 R=52/102=0.5098 F=0.3312

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=314/314=100%
* Multi-token: MWE-based: P=117/314=0.3726 R=117/227=0.5154 F=0.4325
* Single-token: MWE-proportion: gold=1/228=0% pred=0/314=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=314/314=100%
* Seen-in-train: MWE-based: P=117/314=0.3726 R=117/128=0.9141 F=0.5294
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/314=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=244/314=78%
* Variant-of-train: MWE-based: P=67/244=0.2746 R=67/74=0.9054 F=0.4214
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=70/314=22%
* Identical-to-train: MWE-based: P=50/70=0.7143 R=50/54=0.9259 F=0.8065

