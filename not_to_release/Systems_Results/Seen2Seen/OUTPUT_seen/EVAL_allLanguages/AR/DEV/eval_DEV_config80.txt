## Global evaluation
* MWE-based: P=70/90=0.7778 R=70/228=0.3070 F=0.4403
* Tok-based: P=155/195=0.7949 R=155/498=0.3112 F=0.4473

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=0/90=0%
* IAV: MWE-based: P=0/0=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=0/0=0.0000 R=0/76=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=15/228=7% pred=7/90=8%
* LVC.cause: MWE-based: P=5/7=0.7143 R=5/15=0.3333 F=0.4545
* LVC.cause: Tok-based: P=10/14=0.7143 R=10/33=0.3030 F=0.4255
* LVC.full: MWE-proportion: gold=121/228=53% pred=60/90=67%
* LVC.full: MWE-based: P=43/60=0.7167 R=43/121=0.3554 F=0.4751
* LVC.full: Tok-based: P=98/133=0.7368 R=98/266=0.3684 F=0.4912
* VID: MWE-proportion: gold=54/228=24% pred=23/90=26%
* VID: MWE-based: P=17/23=0.7391 R=17/54=0.3148 F=0.4416
* VID: Tok-based: P=36/48=0.7500 R=36/123=0.2927 F=0.4211

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=61/90=68%
* Continuous: MWE-based: P=48/61=0.7869 R=48/126=0.3810 F=0.5134
* Discontinuous: MWE-proportion: gold=102/228=45% pred=29/90=32%
* Discontinuous: MWE-based: P=22/29=0.7586 R=22/102=0.2157 F=0.3359

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=90/90=100%
* Multi-token: MWE-based: P=70/90=0.7778 R=70/227=0.3084 F=0.4416
* Single-token: MWE-proportion: gold=1/228=0% pred=0/90=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=90/90=100%
* Seen-in-train: MWE-based: P=70/90=0.7778 R=70/128=0.5469 F=0.6422
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/90=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=48/90=53%
* Variant-of-train: MWE-based: P=38/48=0.7917 R=38/74=0.5135 F=0.6230
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=42/90=47%
* Identical-to-train: MWE-based: P=32/42=0.7619 R=32/54=0.5926 F=0.6667

