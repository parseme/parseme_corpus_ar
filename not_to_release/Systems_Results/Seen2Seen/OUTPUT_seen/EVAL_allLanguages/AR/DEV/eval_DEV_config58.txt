## Global evaluation
* MWE-based: P=84/127=0.6614 R=84/228=0.3684 F=0.4732
* Tok-based: P=187/275=0.6800 R=187/498=0.3755 F=0.4838

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=1/127=1%
* IAV: MWE-based: P=0/1=0.0000 R=0/38=0.0000 F=0.0000
* IAV: Tok-based: P=1/2=0.5000 R=1/76=0.0132 F=0.0256
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/127=6%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=13/17=0.7647 R=13/33=0.3939 F=0.5200
* LVC.full: MWE-proportion: gold=121/228=53% pred=79/127=62%
* LVC.full: MWE-based: P=49/79=0.6203 R=49/121=0.4050 F=0.4900
* LVC.full: Tok-based: P=110/173=0.6358 R=110/266=0.4135 F=0.5011
* MVC: MWE-proportion: gold=0/228=0% pred=1/127=1%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=38/127=30%
* VID: MWE-based: P=24/38=0.6316 R=24/54=0.4444 F=0.5217
* VID: Tok-based: P=52/81=0.6420 R=52/123=0.4228 F=0.5098

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=64/127=50%
* Continuous: MWE-based: P=50/64=0.7812 R=50/126=0.3968 F=0.5263
* Discontinuous: MWE-proportion: gold=102/228=45% pred=63/127=50%
* Discontinuous: MWE-based: P=34/63=0.5397 R=34/102=0.3333 F=0.4121

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=127/127=100%
* Multi-token: MWE-based: P=84/127=0.6614 R=84/227=0.3700 F=0.4746
* Single-token: MWE-proportion: gold=1/228=0% pred=0/127=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=127/127=100%
* Seen-in-train: MWE-based: P=84/127=0.6614 R=84/128=0.6562 F=0.6588
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/127=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=84/127=66%
* Variant-of-train: MWE-based: P=51/84=0.6071 R=51/74=0.6892 F=0.6456
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=43/127=34%
* Identical-to-train: MWE-based: P=33/43=0.7674 R=33/54=0.6111 F=0.6804

