## Global evaluation
* MWE-based: P=100/153=0.6536 R=100/228=0.4386 F=0.5249
* Tok-based: P=223/323=0.6904 R=223/498=0.4478 F=0.5432

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=55/153=36%
* IAV: MWE-based: P=26/55=0.4727 R=26/38=0.6842 F=0.5591
* IAV: Tok-based: P=53/110=0.4818 R=53/76=0.6974 F=0.5699
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/153=5%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=12/16=0.7500 R=12/33=0.3636 F=0.4898
* LVC.full: MWE-proportion: gold=121/228=53% pred=66/153=43%
* LVC.full: MWE-based: P=45/66=0.6818 R=45/121=0.3719 F=0.4813
* LVC.full: Tok-based: P=105/146=0.7192 R=105/266=0.3947 F=0.5097
* VID: MWE-proportion: gold=54/228=24% pred=24/153=16%
* VID: MWE-based: P=18/24=0.7500 R=18/54=0.3333 F=0.4615
* VID: Tok-based: P=38/51=0.7451 R=38/123=0.3089 F=0.4368

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=99/153=65%
* Continuous: MWE-based: P=66/99=0.6667 R=66/126=0.5238 F=0.5867
* Discontinuous: MWE-proportion: gold=102/228=45% pred=54/153=35%
* Discontinuous: MWE-based: P=34/54=0.6296 R=34/102=0.3333 F=0.4359

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=153/153=100%
* Multi-token: MWE-based: P=100/153=0.6536 R=100/227=0.4405 F=0.5263
* Single-token: MWE-proportion: gold=1/228=0% pred=0/153=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=153/153=100%
* Seen-in-train: MWE-based: P=100/153=0.6536 R=100/128=0.7812 F=0.7117
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/153=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=83/153=54%
* Variant-of-train: MWE-based: P=48/83=0.5783 R=48/74=0.6486 F=0.6115
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=70/153=46%
* Identical-to-train: MWE-based: P=52/70=0.7429 R=52/54=0.9630 F=0.8387

