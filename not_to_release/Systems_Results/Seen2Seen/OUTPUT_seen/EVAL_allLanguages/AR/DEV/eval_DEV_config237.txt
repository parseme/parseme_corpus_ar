## Global evaluation
* MWE-based: P=120/385=0.3117 R=120/228=0.5263 F=0.3915
* Tok-based: P=286/852=0.3357 R=286/498=0.5743 F=0.4237

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=156/385=41%
* IAV: MWE-based: P=32/156=0.2051 R=32/38=0.8421 F=0.3299
* IAV: Tok-based: P=64/312=0.2051 R=64/76=0.8421 F=0.3299
* LVC.cause: MWE-proportion: gold=15/228=7% pred=11/385=3%
* LVC.cause: MWE-based: P=6/11=0.5455 R=6/15=0.4000 F=0.4615
* LVC.cause: Tok-based: P=13/24=0.5417 R=13/33=0.3939 F=0.4561
* LVC.full: MWE-proportion: gold=121/228=53% pred=148/385=38%
* LVC.full: MWE-based: P=51/148=0.3446 R=51/121=0.4215 F=0.3792
* LVC.full: Tok-based: P=134/354=0.3785 R=134/266=0.5038 F=0.4323
* MVC: MWE-proportion: gold=0/228=0% pred=1/385=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=69/385=18%
* VID: MWE-based: P=26/69=0.3768 R=26/54=0.4815 F=0.4228
* VID: Tok-based: P=60/160=0.3750 R=60/123=0.4878 F=0.4240

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=107/385=28%
* Continuous: MWE-based: P=68/107=0.6355 R=68/126=0.5397 F=0.5837
* Discontinuous: MWE-proportion: gold=102/228=45% pred=278/385=72%
* Discontinuous: MWE-based: P=52/278=0.1871 R=52/102=0.5098 F=0.2737

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=385/385=100%
* Multi-token: MWE-based: P=120/385=0.3117 R=120/227=0.5286 F=0.3922
* Single-token: MWE-proportion: gold=1/228=0% pred=0/385=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=385/385=100%
* Seen-in-train: MWE-based: P=120/385=0.3117 R=120/128=0.9375 F=0.4678
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/385=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=309/385=80%
* Variant-of-train: MWE-based: P=67/309=0.2168 R=67/74=0.9054 F=0.3499
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=76/385=20%
* Identical-to-train: MWE-based: P=53/76=0.6974 R=53/54=0.9815 F=0.8154

