## Global evaluation
* MWE-based: P=107/165=0.6485 R=107/228=0.4693 F=0.5445
* Tok-based: P=238/349=0.6819 R=238/498=0.4779 F=0.5620

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=56/165=34%
* IAV: MWE-based: P=27/56=0.4821 R=27/38=0.7105 F=0.5745
* IAV: Tok-based: P=55/112=0.4911 R=55/76=0.7237 F=0.5851
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/165=5%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=12/16=0.7500 R=12/33=0.3636 F=0.4898
* LVC.full: MWE-proportion: gold=121/228=53% pred=74/165=45%
* LVC.full: MWE-based: P=50/74=0.6757 R=50/121=0.4132 F=0.5128
* LVC.full: Tok-based: P=116/163=0.7117 R=116/266=0.4361 F=0.5408
* VID: MWE-proportion: gold=54/228=24% pred=27/165=16%
* VID: MWE-based: P=19/27=0.7037 R=19/54=0.3519 F=0.4691
* VID: Tok-based: P=40/58=0.6897 R=40/123=0.3252 F=0.4420

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=103/165=62%
* Continuous: MWE-based: P=69/103=0.6699 R=69/126=0.5476 F=0.6026
* Discontinuous: MWE-proportion: gold=102/228=45% pred=62/165=38%
* Discontinuous: MWE-based: P=38/62=0.6129 R=38/102=0.3725 F=0.4634

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=165/165=100%
* Multi-token: MWE-based: P=107/165=0.6485 R=107/227=0.4714 F=0.5459
* Single-token: MWE-proportion: gold=1/228=0% pred=0/165=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=165/165=100%
* Seen-in-train: MWE-based: P=107/165=0.6485 R=107/128=0.8359 F=0.7304
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/165=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=93/165=56%
* Variant-of-train: MWE-based: P=53/93=0.5699 R=53/74=0.7162 F=0.6347
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=72/165=44%
* Identical-to-train: MWE-based: P=54/72=0.7500 R=54/54=1.0000 F=0.8571

