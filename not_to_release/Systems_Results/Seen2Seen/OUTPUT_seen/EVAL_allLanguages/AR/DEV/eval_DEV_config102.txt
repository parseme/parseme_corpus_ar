## Global evaluation
* MWE-based: P=120/297=0.4040 R=120/228=0.5263 F=0.4571
* Tok-based: P=293/641=0.4571 R=293/498=0.5884 F=0.5145

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=97/297=33%
* IAV: MWE-based: P=30/97=0.3093 R=30/38=0.7895 F=0.4444
* IAV: Tok-based: P=61/194=0.3144 R=61/76=0.8026 F=0.4519
* LVC.cause: MWE-proportion: gold=15/228=7% pred=10/297=3%
* LVC.cause: MWE-based: P=6/10=0.6000 R=6/15=0.4000 F=0.4800
* LVC.cause: Tok-based: P=13/22=0.5909 R=13/33=0.3939 F=0.4727
* LVC.full: MWE-proportion: gold=121/228=53% pred=135/297=45%
* LVC.full: MWE-based: P=57/135=0.4222 R=57/121=0.4711 F=0.4453
* LVC.full: Tok-based: P=147/301=0.4884 R=147/266=0.5526 F=0.5185
* MVC: MWE-proportion: gold=0/228=0% pred=1/297=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=54/297=18%
* VID: MWE-based: P=22/54=0.4074 R=22/54=0.4074 F=0.4074
* VID: Tok-based: P=56/122=0.4590 R=56/123=0.4553 F=0.4571

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=101/297=34%
* Continuous: MWE-based: P=65/101=0.6436 R=65/126=0.5159 F=0.5727
* Discontinuous: MWE-proportion: gold=102/228=45% pred=196/297=66%
* Discontinuous: MWE-based: P=55/196=0.2806 R=55/102=0.5392 F=0.3691

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=297/297=100%
* Multi-token: MWE-based: P=120/297=0.4040 R=120/227=0.5286 F=0.4580
* Single-token: MWE-proportion: gold=1/228=0% pred=0/297=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=297/297=100%
* Seen-in-train: MWE-based: P=120/297=0.4040 R=120/128=0.9375 F=0.5647
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/297=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=230/297=77%
* Variant-of-train: MWE-based: P=71/230=0.3087 R=71/74=0.9595 F=0.4671
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=67/297=23%
* Identical-to-train: MWE-based: P=49/67=0.7313 R=49/54=0.9074 F=0.8099

