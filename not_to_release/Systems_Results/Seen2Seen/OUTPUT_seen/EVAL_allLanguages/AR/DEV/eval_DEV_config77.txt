## Global evaluation
* MWE-based: P=101/159=0.6352 R=101/228=0.4430 F=0.5220
* Tok-based: P=225/335=0.6716 R=225/498=0.4518 F=0.5402

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=57/159=36%
* IAV: MWE-based: P=27/57=0.4737 R=27/38=0.7105 F=0.5684
* IAV: Tok-based: P=55/114=0.4825 R=55/76=0.7237 F=0.5789
* LVC.cause: MWE-proportion: gold=15/228=7% pred=7/159=4%
* LVC.cause: MWE-based: P=5/7=0.7143 R=5/15=0.3333 F=0.4545
* LVC.cause: Tok-based: P=10/14=0.7143 R=10/33=0.3030 F=0.4255
* LVC.full: MWE-proportion: gold=121/228=53% pred=68/159=43%
* LVC.full: MWE-based: P=45/68=0.6618 R=45/121=0.3719 F=0.4762
* LVC.full: Tok-based: P=105/150=0.7000 R=105/266=0.3947 F=0.5048
* VID: MWE-proportion: gold=54/228=24% pred=27/159=17%
* VID: MWE-based: P=19/27=0.7037 R=19/54=0.3519 F=0.4691
* VID: Tok-based: P=40/57=0.7018 R=40/123=0.3252 F=0.4444

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=102/159=64%
* Continuous: MWE-based: P=68/102=0.6667 R=68/126=0.5397 F=0.5965
* Discontinuous: MWE-proportion: gold=102/228=45% pred=57/159=36%
* Discontinuous: MWE-based: P=33/57=0.5789 R=33/102=0.3235 F=0.4151

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=159/159=100%
* Multi-token: MWE-based: P=101/159=0.6352 R=101/227=0.4449 F=0.5233
* Single-token: MWE-proportion: gold=1/228=0% pred=0/159=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=159/159=100%
* Seen-in-train: MWE-based: P=101/159=0.6352 R=101/128=0.7891 F=0.7038
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/159=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=85/159=53%
* Variant-of-train: MWE-based: P=48/85=0.5647 R=48/74=0.6486 F=0.6038
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=74/159=47%
* Identical-to-train: MWE-based: P=53/74=0.7162 R=53/54=0.9815 F=0.8281

