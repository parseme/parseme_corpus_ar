## Global evaluation
* MWE-based: P=120/363=0.3306 R=120/228=0.5263 F=0.4061
* Tok-based: P=286/807=0.3544 R=286/498=0.5743 F=0.4383

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=137/363=38%
* IAV: MWE-based: P=32/137=0.2336 R=32/38=0.8421 F=0.3657
* IAV: Tok-based: P=64/274=0.2336 R=64/76=0.8421 F=0.3657
* LVC.cause: MWE-proportion: gold=15/228=7% pred=11/363=3%
* LVC.cause: MWE-based: P=6/11=0.5455 R=6/15=0.4000 F=0.4615
* LVC.cause: Tok-based: P=13/24=0.5417 R=13/33=0.3939 F=0.4561
* LVC.full: MWE-proportion: gold=121/228=53% pred=147/363=40%
* LVC.full: MWE-based: P=51/147=0.3469 R=51/121=0.4215 F=0.3806
* LVC.full: Tok-based: P=134/352=0.3807 R=134/266=0.5038 F=0.4337
* MVC: MWE-proportion: gold=0/228=0% pred=1/363=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=54/228=24% pred=67/363=18%
* VID: MWE-based: P=26/67=0.3881 R=26/54=0.4815 F=0.4298
* VID: Tok-based: P=60/155=0.3871 R=60/123=0.4878 F=0.4317

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=104/363=29%
* Continuous: MWE-based: P=68/104=0.6538 R=68/126=0.5397 F=0.5913
* Discontinuous: MWE-proportion: gold=102/228=45% pred=259/363=71%
* Discontinuous: MWE-based: P=52/259=0.2008 R=52/102=0.5098 F=0.2881

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=363/363=100%
* Multi-token: MWE-based: P=120/363=0.3306 R=120/227=0.5286 F=0.4068
* Single-token: MWE-proportion: gold=1/228=0% pred=0/363=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=363/363=100%
* Seen-in-train: MWE-based: P=120/363=0.3306 R=120/128=0.9375 F=0.4888
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/363=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=289/363=80%
* Variant-of-train: MWE-based: P=67/289=0.2318 R=67/74=0.9054 F=0.3691
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=74/363=20%
* Identical-to-train: MWE-based: P=53/74=0.7162 R=53/54=0.9815 F=0.8281

