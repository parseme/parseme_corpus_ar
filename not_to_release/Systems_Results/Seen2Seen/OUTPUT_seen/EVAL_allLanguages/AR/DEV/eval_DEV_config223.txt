## Global evaluation
* MWE-based: P=107/170=0.6294 R=107/228=0.4693 F=0.5377
* Tok-based: P=238/359=0.6630 R=238/498=0.4779 F=0.5554

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=38/228=17% pred=57/170=34%
* IAV: MWE-based: P=27/57=0.4737 R=27/38=0.7105 F=0.5684
* IAV: Tok-based: P=55/114=0.4825 R=55/76=0.7237 F=0.5789
* LVC.cause: MWE-proportion: gold=15/228=7% pred=8/170=5%
* LVC.cause: MWE-based: P=6/8=0.7500 R=6/15=0.4000 F=0.5217
* LVC.cause: Tok-based: P=12/16=0.7500 R=12/33=0.3636 F=0.4898
* LVC.full: MWE-proportion: gold=121/228=53% pred=76/170=45%
* LVC.full: MWE-based: P=50/76=0.6579 R=50/121=0.4132 F=0.5076
* LVC.full: Tok-based: P=116/167=0.6946 R=116/266=0.4361 F=0.5358
* VID: MWE-proportion: gold=54/228=24% pred=29/170=17%
* VID: MWE-based: P=19/29=0.6552 R=19/54=0.3519 F=0.4578
* VID: Tok-based: P=40/62=0.6452 R=40/123=0.3252 F=0.4324

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=126/228=55% pred=104/170=61%
* Continuous: MWE-based: P=69/104=0.6635 R=69/126=0.5476 F=0.6000
* Discontinuous: MWE-proportion: gold=102/228=45% pred=66/170=39%
* Discontinuous: MWE-based: P=38/66=0.5758 R=38/102=0.3725 F=0.4524

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=227/228=100% pred=170/170=100%
* Multi-token: MWE-based: P=107/170=0.6294 R=107/227=0.4714 F=0.5390
* Single-token: MWE-proportion: gold=1/228=0% pred=0/170=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=128/228=56% pred=170/170=100%
* Seen-in-train: MWE-based: P=107/170=0.6294 R=107/128=0.8359 F=0.7181
* Unseen-in-train: MWE-proportion: gold=100/228=44% pred=0/170=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/100=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=74/128=58% pred=95/170=56%
* Variant-of-train: MWE-based: P=53/95=0.5579 R=53/74=0.7162 F=0.6272
* Identical-to-train: MWE-proportion: gold=54/128=42% pred=75/170=44%
* Identical-to-train: MWE-based: P=54/75=0.7200 R=54/54=1.0000 F=0.8372

